# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

# CKAN metadata is available under the CC-0 license

from common.ckan import CKAN, Install
from pybuild.info import P


class Package(CKAN):
    NAME = "Ferram Aerospace Research Continued"
    DESC = "FAR replaces KSP's stock part-centered aerodynamics model with one based on real-life physics."
    LICENSE = "GPL-3"
    HOMEPAGE = "https://forum.kerbalspaceprogram.com/index.php?/topic/179445-* https://github.com/dkavolis/Ferram-Aerospace-Research"
    KEYWORDS = "ksp ksp-1.8 ksp-1.9"
    RDEPEND = """ModuleManager
        ModularFlightIntegrator
        !!ckan/FerramAerospaceResearch-0.14.3.2
        !!ckan/FerramAerospaceResearch-0.14.4
        !!ckan/FerramAerospaceResearch-0.14.5
        !!ckan/FerramAerospaceResearch-0.14.5.1
        !!ckan/FerramAerospaceResearch-0.14.6
        !!ckan/FerramAerospaceResearch-0.14.7
        !!ckan/FerramAerospaceResearch-0.15.0
        !!ckan/FerramAerospaceResearch-0.15.1
        !!ckan/FerramAerospaceResearch-0.15.2
        !!ckan/FerramAerospaceResearch-0.15.3
        !!ckan/FerramAerospaceResearch-e1-0.15.3.1
        !!ckan/FerramAerospaceResearch-e1-0.15.4
        !!ckan/FerramAerospaceResearch-e2-0.15.4.1
        !!ckan/FerramAerospaceResearch-e3-0.15.5
        !!ckan/FerramAerospaceResearch-e3-0.15.5.1
        !!ckan/FerramAerospaceResearch-e3-0.15.5.2
        !!ckan/FerramAerospaceResearch-e3-0.15.5.3
        !!ckan/FerramAerospaceResearch-e3-0.15.5.4
        !!ckan/FerramAerospaceResearch-e3-0.15.5.5
        !!ckan/FerramAerospaceResearch-e3-0.15.5.6
        !!ckan/FerramAerospaceResearch-e3-0.15.5.7
        !!ckan/FerramAerospaceResearch-e3-0.15.6
        !!ckan/FerramAerospaceResearch-e3-0.15.6.1
        !!ckan/FerramAerospaceResearch-e3-0.15.6.2
        !!ckan/FerramAerospaceResearch-e3-0.15.6.3
        !!ckan/FerramAerospaceResearch-e3-0.15.6.4
        !!ckan/FerramAerospaceResearch-e3-0.15.6.5
        !!ckan/FerramAerospaceResearch-e3-0.15.7.1
        !!ckan/FerramAerospaceResearch-e3-0.15.7.2
        !!ckan/FerramAerospaceResearch-e3-0.15.8
        !!ckan/FerramAerospaceResearch-e3-0.15.8.1
        !!ckan/FerramAerospaceResearch-e3-0.15.9
        !!ckan/FerramAerospaceResearch-e3-0.15.9.1
        !!ckan/FerramAerospaceResearchContinued-e3-0.15.10.0
        !!ckan/FerramAerospaceResearchContinued-e3-0.15.10.1
        !!ckan/FerramAerospaceResearchContinued-e3-0.15.11.0
        !!ckan/FerramAerospaceResearchContinued-e3-0.15.11.1
        !!ckan/FerramAerospaceResearchContinued-e3-0.15.11.2
        !!ckan/FerramAerospaceResearchContinued-e3-0.15.11.3
        !!ckan/FerramAerospaceResearchContinued-e3-0.15.9.5
        !!ckan/FerramAerospaceResearchContinued-e3-0.15.9.6
        !!ckan/FerramAerospaceResearchContinued-e3-0.15.9.7
        !!ckan/FerramAerospaceResearchContinued-e3-0.16.0.0
        !!ckan/FerramAerospaceResearchContinued-e3-0.16.0.1
        !!ckan/FerramAerospaceResearchContinued-e3-0.16.0.2
        !!ckan/FerramAerospaceResearchContinued-e3-0.16.0.3
        !!ckan/FerramAerospaceResearchContinued-e3-0.16.0.4
        !!ckan/FerramAerospaceResearchContinued-e3-0.16.0.5
        !!ckan/FerramAerospaceResearchContinued-e3-0.16.1.0
        !!ckan/FerramAerospaceResearchContinued-e3-0.16.1.1
        !!ckan/FerramAerospaceResearchContinued-e3-0.16.1.2
        !!ckan/NEAR-1.3.1
        !!ckan/NEAR-1.3a"""
    SRC_URI = f"https://github.com/dkavolis/Ferram-Aerospace-Research/releases/download/v0.15.11.4_Mach_/FAR_0_15_11_4_Mach.zip -> {P}.zip"
    INSTALL = [
        Install(
            filter=["config.xml"],
            install_to="GameData",
            path="GameData/FerramAerospaceResearch",
        ),
        Install(install_to="Ships", path="Ships"),
    ]
