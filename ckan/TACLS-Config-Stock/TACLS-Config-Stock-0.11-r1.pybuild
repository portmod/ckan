# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

# CKAN metadata is available under the CC-0 license

from common.ckan import CKAN, Install
from pybuild.info import P


class Package(CKAN):
    NAME = "TAC Life Support (TACLS) - stock config"
    DESC = "Stock configuration for TACLS"
    LICENSE = "CC-BY-NC-SA-3.0"
    HOMEPAGE = "https://github.com/taraniselsu/TacLifeSupport"
    KEYWORDS = "ksp ksp-1.0"
    RDEPEND = """TACLS
        !!ckan/TACLS-Config-RealismOverhaul-10.0.0
        !!ckan/TACLS-Config-RealismOverhaul-10.1.0
        !!ckan/TACLS-Config-RealismOverhaul-10.2.0
        !!ckan/TACLS-Config-RealismOverhaul-10.3.0
        !!ckan/TACLS-Config-RealismOverhaul-10.3.1
        !!ckan/TACLS-Config-RealismOverhaul-10.4.0
        !!ckan/TACLS-Config-RealismOverhaul-10.4.1
        !!ckan/TACLS-Config-RealismOverhaul-10.5.0
        !!ckan/TACLS-Config-RealismOverhaul-10.6.0
        !!ckan/TACLS-Config-RealismOverhaul-10.7.0
        !!ckan/TACLS-Config-RealismOverhaul-10.7.1
        !!ckan/TACLS-Config-RealismOverhaul-10.7.2
        !!ckan/TACLS-Config-RealismOverhaul-10.7.3
        !!ckan/TACLS-Config-RealismOverhaul-10.8.0
        !!ckan/TACLS-Config-RealismOverhaul-10.9.0
        !!ckan/TACLS-Config-RealismOverhaul-10.9.1
        !!ckan/TACLS-Config-RealismOverhaul-10.9.2
        !!ckan/TACLS-Config-RealismOverhaul-10.9.3
        !!ckan/TACLS-Config-RealismOverhaul-10.9.4
        !!ckan/TACLS-Config-RealismOverhaul-10.9.5
        !!ckan/TACLS-Config-RealismOverhaul-10.9.6
        !!ckan/TACLS-Config-RealismOverhaul-11.0.0
        !!ckan/TACLS-Config-RealismOverhaul-11.0.1
        !!ckan/TACLS-Config-RealismOverhaul-11.1.0
        !!ckan/TACLS-Config-RealismOverhaul-11.2.0
        !!ckan/TACLS-Config-RealismOverhaul-11.3.0
        !!ckan/TACLS-Config-RealismOverhaul-11.3.1
        !!ckan/TACLS-Config-RealismOverhaul-11.3.2
        !!ckan/TACLS-Config-RealismOverhaul-6.1.2c
        !!ckan/TACLS-Config-RealismOverhaul-7.0.0
        !!ckan/TACLS-Config-RealismOverhaul-7.0.0a
        !!ckan/TACLS-Config-RealismOverhaul-7.0.0b
        !!ckan/TACLS-Config-RealismOverhaul-7.0.1
        !!ckan/TACLS-Config-RealismOverhaul-7.0.2
        !!ckan/TACLS-Config-RealismOverhaul-7.0.3
        !!ckan/TACLS-Config-RealismOverhaul-7.0.4
        !!ckan/TACLS-Config-RealismOverhaul-7.0.5
        !!ckan/TACLS-Config-RealismOverhaul-7.0.6
        !!ckan/TACLS-Config-RealismOverhaul-7.0.7
        !!ckan/TACLS-Config-RealismOverhaul-8.0.0
        !!ckan/TACLS-Config-RealismOverhaul-8.0.1
        !!ckan/TACLS-Config-RealismOverhaul-8.1.0
        !!ckan/TACLS-Config-RealismOverhaul-8.1.1
        !!ckan/TACLS-Config-RealismOverhaul-8.2.0
        !!ckan/TACLS-Config-RealismOverhaul-8.3.0
        !!ckan/TACLS-Config-RealismOverhaul-8.3.1
        !!ckan/TACLS-Config-RealismOverhaul-8.4.0
        !!ckan/TACLS-Config-RealismOverhaul-8.4.1
        !!ckan/TACLS-Config-Stock-0.10.1
        !!ckan/TACLS-Config-Stock-0.10.2b
        !!ckan/TACLS-Config-Stock-0.10.3
        !!ckan/TACLS-Config-Stock-0.11.1
        !!ckan/TACLS-Config-Stock-0.11.1.20
        !!ckan/TACLS-Config-Stock-0.11.2
        !!ckan/TACLS-Config-Stock-0.11.3
        !!ckan/TACLS-Config-Stock-0.12.0
        !!ckan/TACLS-Config-Stock-0.12.1
        !!ckan/TACLS-Config-Stock-0.12.2
        !!ckan/TACLS-Config-Stock-0.12.3
        !!ckan/TACLS-Config-Stock-0.12.4"""
    SRC_URI = f"https://github.com/taraniselsu/TacLifeSupport/releases/download/v0.11/TacLifeSupport_0.11.0.18.zip -> {P}.zip"
    INSTALL = [
        Install(
            filter_regexp="^(?!GameData/ThunderAerospace/TacLifeSupport/PluginData/TacLifeSupport/LifeSupport.cfg)",
            install_to="GameData",
            path="GameData/ThunderAerospace",
        )
    ]
