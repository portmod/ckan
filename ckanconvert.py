# Copyright 2021-2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import json
import mimetypes
import os
import shutil
from functools import lru_cache
from collections import defaultdict
from glob import glob, iglob
from logging import warning
from multiprocessing import Pool, Value
from types import SimpleNamespace
from typing import Dict, List, Optional, Set, Tuple, cast
from subprocess import check_output

import black
from importmod.atom import parse_atom
from importmod.generate import create_metadata
from portmod.parsers.manifest import FileType, Manifest, ManifestFile
from portmod.pybuild import Pybuild
from portmod.source import HashAlg
from portmod.util import get_newest
from portmodlib.atom import Atom, FQAtom, atom_sat
from portmodlib.parsers.list import write_list
from progressbar import ProgressBar

ROOT = "."
CKAN_meta = ".CKAN-meta"


def get_revisions():
    global FILE_REVISIONS
    files = check_output(
        ["git", "log", "--pretty=", "--name-only"], cwd=CKAN_meta, encoding="utf-8"
    ).splitlines()
    FILE_REVISIONS = defaultdict(int)
    for file in files:
        FILE_REVISIONS[file] += 1


@lru_cache()
def parse_version(version: str, filename: Optional[str] = None) -> str:
    from importmod.atom import parse_version as imp_parse_version

    revision = 0
    if filename:
        path = os.path.relpath(filename, start=CKAN_meta)
        revision = FILE_REVISIONS[path]
        assert revision >= 0

    if ":" in version:
        epoch, _, version = version.partition(":")
        version = imp_parse_version(version)
        # If the version is not parseable, don't return a mangled 'e1-' version
        if not version:
            return ""
        result = "e" + epoch + "-" + version
    else:
        result = imp_parse_version(version)
        if not result:
            return ""
    if revision:
        return result + f"-r{revision}"
    return result


class Install(SimpleNamespace):
    def __str__(self):
        items = (f"{k}={v!r}" for k, v in sorted(self.__dict__.items()))
        return "{}({})".format(type(self).__name__, ", ".join(items))


def iterate_packages():
    for package_name in os.listdir(CKAN_meta):
        yield os.path.join(os.path.join(CKAN_meta, package_name))


def total_packages():
    for package_dir in iterate_packages():
        yield from iglob(os.path.join(package_dir, "*.ckan"))
        yield from iglob(os.path.join(package_dir, "*.kerbalstuff"))
        yield from iglob(os.path.join(package_dir, "*.frozen"))


total = Value("i", 0)

# Any license renames necessary
LICENSES = {
    "LGPL-2.0": "LGPL-2",
    "LGPL-3.0": "LGPL-3",
    "GPL-3.0": "GPL-3",
    "GPL-2.0": "GPL-2",
    "GPL-1.0": "GPL-1",
    "restricted": "all-rights-reserved",
    "BSD-2-clause": "BSD-2",
    "BSD-3-clause": "BSD",
    "BSD-4-clause": "BSD-4",
    "CC0": "CC0-1.0",
    "unknown": "all-rights-reserved",
}


def get_ksp_versions(data: Dict) -> Set[Tuple[int, int]]:
    results = set()

    def acceptable_version(version: str):
        # Don't track version numbers that only include the major version
        # or versions of KSP prior to 1.0
        return version != "any" and "." in version and not version.startswith("0.")

    if "ksp_version" in data:
        version = data["ksp_version"]
        if acceptable_version(version):
            results.add(tuple(map(int, version.split(".")[:2])))
    if "ksp_version_min" in data:
        version = data["ksp_version_min"]
        if acceptable_version(version) and "99" not in version.split("."):
            results.add(tuple(map(int, version.split(".")[:2])))
    if "ksp_version_max" in data:
        version = data["ksp_version_max"]
        if acceptable_version(version) and "99" not in version.split("."):
            results.add(tuple(map(int, version.split(".")[:2])))
    return cast(Set[Tuple[int, int]], results)


def get_filenames(package_dir: str):
    return (
        glob(os.path.join(package_dir, "*.ckan"))
        + glob(os.path.join(package_dir, "*.kerbalstuff"))
        + glob(os.path.join(package_dir, "*.frozen"))
    )


def process_files(package_dir: str):
    filenames = get_filenames(package_dir)
    category = "ckan"
    files = {}
    for filename in filenames:
        with open(filename) as file:
            data = json.load(file)
        files[filename] = data

    processed_packages = {}
    for filename, data in files.items():
        pkg = process_file(filename, data, category)
        if pkg:
            processed_packages[pkg] = data

    if processed_packages:
        newest = get_newest(processed_packages)
        newest_data = processed_packages[newest]

        longdescription = newest_data.get("description")
        if not longdescription and (
            len(newest_data.get("abstract", "").splitlines()) > 1
            or len(newest_data.get("abstract", "").split(". ")) > 1
        ):
            longdescription = newest_data["abstract"]

        # Use author, description (longdescription), resources/bugtracker and resources/manual to create metadata.yaml
        authors = newest_data.get("author")
        # Make sure that author is not an integer, as this will break parsing the yaml.
        # Apparently this sometimes is the case
        if authors:
            if isinstance(authors, str):
                try:
                    int(authors)
                    authors = [f'"{authors}"']
                except ValueError:
                    authors = [authors]
            elif isinstance(authors, list):
                newlist = []
                for elem in authors:
                    try:
                        int(elem)
                        newlist.append(f'"{elem}"')
                    except ValueError:
                        newlist.append(elem)
                authors = newlist

        metadata_path = os.path.join(ROOT, newest.CATEGORY, newest.PN, "metadata.yaml")
        create_metadata(
            metadata_path,
            authors=authors,
            longdescription=longdescription,
            bugs_to=newest_data.get("resources", {}).get("bugtracker"),
            doc=newest_data.get("resources", {}).get("manual"),
            maintainer="CKAN",
            tags=newest_data.get("tags")
        )


def get_atom(data: Dict, category: str, filename: Optional[str] = None):
    version = parse_version(data["version"], filename)
    if not version:
        version = "0"
        warning(
            f'Skipping version for package {data["identifier"]} with version "{data["version"]}"'
        )

    # FIXME: atoms can't end in anything resembling a version, but some ckan packages do.
    # They should be remapped to versions without a hyphen before the non-version numerical component
    # if re.match("\-[0-9]*", data[identifier]):
    # All references will also need to be updated.
    return FQAtom(category + "/" + data["identifier"] + "-" + version + "::ckan")


def get_provides(filename: str, data: Dict, category: str) -> Dict[str, str]:
    virtual_map = {}
    atom = get_atom(data, category)
    # Generate virtual packages based on "provides"
    for virtual in data.get("provides", []):
        virtual_map[virtual] = atom.CP

    return virtual_map


def process_file(filename: str, data: Dict, category: str):
    global total
    with total.get_lock():
        total.value += 1
        bar.update(total.value)

    try:
        atom = get_atom(data, category, filename=filename)
        pkg = Pybuild(
            atom,
            FILE="<none>",
            REPO="ckan",
        )
        pkg.NAME = data["name"]
        if data.get("abstract"):
            pkg.DESC = data["abstract"].splitlines()[
                0
            ]  # If abstract is multiple lines, only use the first
            # If multiple sentences, only include the first one
            if ". " in pkg.DESC:
                pkg.DESC = pkg.DESC.split(". ")[0]
        if isinstance(data["license"], list):
            pkg.LICENSE = (
                "|| ( "
                + " ".join(
                    [LICENSES.get(license, license) for license in data["license"]]
                )
                + " )"
            )
        else:
            pkg.LICENSE = LICENSES.get(data["license"], data["license"])

        source_file = None
        if "download" in data:
            # download is not required if "kind" is metapackage or dlc
            if "download_content_type" in data:
                extension = mimetypes.guess_extension(data["download_content_type"])
            else:
                _, extension = os.path.splitext(data["download"])
            source_file = pkg.P + extension
            pkg.SRC_URI = data["download"] + " -> {P}" + extension

        homepages = []
        for keyword in [
            "homepage",
            "repository",
            "spacedock",
            "curse",
            "store",
            "steamstore",
        ]:
            if keyword in data.get("resources", {}):
                # Tokens ending in ? break use-conditional parsing.
                # No URL meaningfully ends in a ? (though it's valid)
                homepages.append(data["resources"][keyword].rstrip("?"))
        pkg.HOMEPAGE = " ".join(homepages)

        pkg.INSTALL = []
        if "install" in data:
            for install in data["install"]:
                install_to = install["install_to"]
                if install_to == "GameRoot":
                    install_to = "."

                path = (
                    install.get("file")
                    or install.get("find")
                    or install.get("find_regexp")
                )

                directory = Install(
                    path=path,
                    install_to=install_to,
                )

                if "as" in install:
                    directory.rename = install.get("as", os.path.basename(path))

                if "find" in install:
                    directory.find = True

                if "find_regexp" in install:
                    directory.find_regexp = True

                if "find_matches_files" in install:
                    directory.find_matches_files = True

                if "filter" in install:
                    directory.filter = install["filter"]
                    if isinstance(directory.filter, str):
                        directory.filter = [directory.filter]
                if "filter_regexp" in install:
                    directory.filter_regexp = install["filter_regexp"]
                if "include_only" in install:
                    directory.include_only = install["include_only"]
                    if isinstance(directory.include_only, str):
                        directory.include_only = [directory.include_only]
                if "include_only_regexp" in install:
                    directory.include_only_regexp = install["include_only_regexp"]
                pkg.INSTALL.append(directory)
        else:
            # If no install sections are provided, a CKAN client must find the
            # top-most directory in the archive that matches the module identifier,
            # and install that with a target of GameData.
            directory = Install(path=pkg.PN, find=True, rename=f"GameData/{pkg.PN}")
            pkg.INSTALL.append(directory)

        def get_depends(deplist):
            depends = []
            for dep in deplist:
                if "any_of" in dep:
                    depends.extend(["|| (\n"] + get_depends(dep["any_of"]) + ["\n)"])
                else:
                    atom = dep["name"]
                    if atom in virtuals:
                        atom = f"virtual/{atom}"
                    if "min_version" in dep and "max_version" in dep:
                        depends.append(f"<={atom}-" + parse_version(dep["max_version"]))
                        depends.append(f">={atom}-" + parse_version(dep["min_version"]))
                    elif "min_version" in dep:
                        depends.append(f">={atom}-" + parse_version(dep["min_version"]))
                    elif "max_version" in dep:
                        depends.append(f"<={atom}-" + parse_version(dep["max_version"]))
                    elif "version" in dep:
                        depends.append(f"~{atom}-" + parse_version(dep["version"]))
                    else:
                        depends.append(atom)
            return depends

        def get_conflicts(conflictlist):
            # If an element of the conflict list is a virtual
            # it should block the contents of that virtual other than itself
            conflicts = set()
            for dep in get_depends(conflictlist):
                dep = Atom(dep)
                if dep.PN in data.get("provides", []) or dep.PN == pkg.PN:
                    for virtual in sorted(virtuals.get(dep.PN, [])):
                        if not atom_sat(pkg.ATOM, Atom(virtual)):
                            conflicts.add(f"!!{virtual}")
                else:
                    conflicts.add(f"!!{dep}")
            return sorted(conflicts)

        pkg.RDEPEND = ("\n" + " " * 8).join(
            get_depends(data.get("depends", []))
            + get_conflicts(data.get("conflicts", []))
        )

        def get_keyword(version: str, status: str):
            components = version.split(".")
            keyword = "ksp-" + ".".join(components[:2])
            if status == "stable":
                return keyword
            if status == "testing" or status == "development":
                return "~" + keyword

        def get_keywords(version: str, status: str):
            if status == "stable":
                if version == "any":
                    return "ksp *"
                return "ksp " + get_keyword(version, status)
            if status == "testing" or status == "development":
                if version == "any":
                    return "~ksp ~*"
                return "~ksp " + get_keyword(version, status)

        # Use release_status and ksp_version(_min/_max/_strict) for keywording
        # To simplify things until architecture versioning is supported, only pay attention to the first two version components
        # FIXME: use all of the version information
        # FIXME: if ksp_version_strict is false,
        #       the mod will be installed if the KSP version it targets is
        #       "generally recognised" as being compatible with the KSP
        #       version the user has installed. It is up to the CKAN client
        #       to determine what is "generally recognised" as working.
        status = data.get("release_status", "stable")
        if "ksp_version" in data:
            pkg.KEYWORDS = get_keywords(data["ksp_version"], status)
        else:
            min_components = ksp_versions[0]
            max_components = ksp_versions[-1]
            if "ksp_version_min" in data:
                min_components = cast(
                    Tuple[int, int],
                    tuple(map(int, data["ksp_version_min"].split(".")[:2])),
                )
            if "ksp_version_max" in data:
                max_components = cast(
                    Tuple[int, int],
                    tuple(map(int, data["ksp_version_max"].split(".")[:2])),
                )

            if status == "stable":
                keyword_list = ["ksp"]
            if status == "testing" or status == "development":
                keyword_list = ["~ksp"]

            for version in ksp_versions:
                if max_components >= version >= min_components:
                    keyword = get_keyword(".".join(map(str, version)), status)
                    if keyword:
                        keyword_list.append(keyword)
            pkg.KEYWORDS = " ".join(keyword_list)

        os.makedirs(os.path.join(ROOT, pkg.CATEGORY, pkg.PN), exist_ok=True)

        # Use download_size and download_hash to generate a manifest file without having to download the sources
        if source_file and "download_size" in data:
            manifest = Manifest(os.path.join(ROOT, pkg.CATEGORY, pkg.PN, "Manifest"))
            hashes = {}
            for hash_type, value in data.get("download_hash", {}).items():
                if hash_type == "sha256":
                    hashes[HashAlg.SHA256] = value.lower()
            manifest.add_entry(
                ManifestFile(source_file, FileType.DIST, data["download_size"], hashes)
            )
            manifest.write()
        elif "download" in data:
            warning(
                f'Skipping package {data["identifier"]}-{data["version"]} '
                "as it has a file to download with no manifest information"
            )
            return

        install_dir_strings = ", ".join(str(idir) for idir in pkg.INSTALL)

        comment = ""
        if "comment" in data:
            commentlines = []
            for line in data["comment"].splitlines():
                commentlines.append("# " + line)
            comment = "\n" + "\n".join(commentlines)

        def strf(string: str):
            return string.replace('"', '\\"')

        with open(
            os.path.join(ROOT, pkg.CATEGORY, pkg.PN, pkg.PF + ".pybuild"), "w"
        ) as file:
            output = f'''# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

# CKAN metadata is available under the CC-0 license

from common.ckan import CKAN, Install
from pybuild.info import P
{comment}

class Package(CKAN):
    NAME="{strf(pkg.NAME)}"
    DESC="{strf(pkg.DESC)}"
    LICENSE="{strf(pkg.LICENSE)}"
    HOMEPAGE="{strf(pkg.HOMEPAGE)}"
    KEYWORDS="{pkg.KEYWORDS}"
    RDEPEND="""{strf(pkg.RDEPEND)}"""
    SRC_URI=f"{strf(pkg.SRC_URI)}"
    INSTALL = [{install_dir_strings}]
'''
            # Note: significantly slower. Not useful for testing
            # output = black.format_str(
            #    output, mode=black.Mode(experimental_string_processing=True)
            # )
            file.write(output)
    except Exception as e:
        # Ignore packages which fail to parse.
        warning(f"In file {filename}")
        warning(e)
        raise e

    return pkg


if __name__ == "__main__":
    packages = list(iterate_packages())
    total_files = list(total_packages())
    ksp_versions_set = set()
    virtuals = defaultdict(set)
    get_revisions()

    package_map: Dict[str, str] = {}

    for package in packages:
        filenames = get_filenames(package)
        if not filenames:
            continue
        category = "ckan"
        for filename in filenames:
            with open(filename) as file:
                data = json.load(file)

            atom = get_atom(data, category)
            if atom.PN not in package_map:
                package_map[atom.PN] = atom.CPN

            ksp_versions_set |= get_ksp_versions(data)
            for key, value in get_provides(filename, data, category).items():
                virtuals[key].add(value)

    ksp_versions: List[Tuple[int, int]] = sorted(ksp_versions_set)
    bar = ProgressBar(max_value=len(total_files))
    bar.start()
    with Pool(8) as p:
        results = p.map(process_files, packages)
    bar.finish()

    for virtual in virtuals:
        os.makedirs(os.path.join(ROOT, "virtual", virtual), exist_ok=True)
        atoms = virtuals[virtual]
        # If virtual is also the name of the package, add it to the map
        # since CKAN implicitly makes packages provide themselves
        if virtual in package_map:
            atoms.add(package_map[virtual])

        contents = ("\n" + " " * 8).join(sorted(atoms))
        with open(
            os.path.join(ROOT, "virtual", virtual, virtual + "-0.pybuild"), "w"
        ) as file:
            output = f'''
# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

# CKAN metadata is available under the CC-0 license

from pybuild import Pybuild1

class Package(Pybuild1):
    NAME="Virtual package for {virtual}"
    DESC=""
    KEYWORDS="ksp *"
    RDEPEND="""|| (
        {contents}
    )"""
'''
            file.write(output)
        create_metadata(
            os.path.join(ROOT, "virtual", virtual, "metadata.yaml"),
            maintainer="ckanconvert.py",
        )

    write_list(
        os.path.join(ROOT, "profiles", "arch.list"),
        ["ksp"] + list(map(lambda x: "ksp-" + ".".join(map(str, x)), ksp_versions)),
    )
