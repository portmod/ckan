# The Portmod CKAN package repository

This package repository is generated from [CKAN-meta](https://github.com/KSP-CKAN/CKAN-meta).

Manual package submissions should either go to the [portmod/ksp](https://gitlab.com/portmod/ksp) repository (for packages too complex for CKAN), or to [CKAN-meta](https://github.com/KSP-CKAN)/[NetKAN](https://github.com/KSP-CKAN/NetKAN).
