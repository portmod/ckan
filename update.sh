#!/bin/bash

cd `dirname $0` || exit 1
shopt -s globstar

git clone https://github.com/KSP-CKAN/CKAN-meta .CKAN-meta || (cd .CKAN-meta && git pull && cd ..)

rm -r */
git checkout licenses
mkdir profiles metadata
echo 'masters = "ksp"' > metadata/layout.conf
echo "ckan" > profiles/repo_name
echo "ckan" > profiles/categories
echo "ksp" > profiles/arch.list
python ckanconvert.py || exit

black -q **/*.pybuild
isort -q **/*.pybuild

git add -A .
git commit -m "Automated CKAN import"
